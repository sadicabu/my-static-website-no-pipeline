#!/bin/bash

npm install 
npm install -g gatsby-cli
gatsby build
sed -i "s/%%VERSION%%/$CI_COMMIT_BEFORE_SHA/" ./public/index.html
sed -i "s/%%BRANCH%%/$CI_COMMIT_BRANCH/" ./public/index.html
sed -i "s/%%MSG%%/$CI_COMMIT_MESSAGE/" ./public/index.html
sed -i "s/%%COMM_BRANCH%%/$CI_COMMIT_BRANCH/" ./public/index.html
sed -i "s/%%DEF_BRANCH%%/$CI_DEFAULT_BRANCH/" ./public/index.html
sed -i "s/%%CI_COMMIT_REF_SLUG%%/$CI_COMMIT_REF_SLUG/" ./public/index.html